/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef __UPDWN_FUNCTIONS_OPL_H__
#define __UPDWN_FUNCTIONS_OPL_H__

#include "updwn-enums.h"
#include "updwn-private.h"

void updwn_attachShader (uint program, uint shader);
void updwn_bindAttribLocation (uint program, uint index, const char *name);
void updwn_bindBuffer (UpdwnBufferTarget target, uint buffer);
void updwn_bindBufferBase (UpdwnBufferTarget target, uint index, uint buffer);
void updwn_bindBufferRange (UpdwnBufferTarget target, uint index, uint buffer, signed long offset, signed long size);
void updwn_bindBuffersBase (UpdwnBufferTarget target, uint first, int count, const uint *buffers);
void updwn_bindBuffersRange (UpdwnBufferTarget target, uint first, int count, const uint *buffers, const signed long *offsets, const signed long *sizes);
void updwn_bindTextures (UpdwnTextureTarget first, int count, const uint *textures);
void updwn_bindVertexArray (uint array);
void updwn_bindVertexBuffer (uint bindingindex, uint buffer, signed long offset, int stride);
void updwn_bindVertexBuffers (uint first, int count, const uint *buffers, const signed long *offsets, const int *strides);
void updwn_blendEquationSeparate (UpdwnBlendEquation modeRGB, UpdwnBlendEquation modeAlpha);
void updwn_blendEquationSeparatei (uint buf, UpdwnBlendEquation modeRGB, UpdwnBlendEquation modeAlpha);
void updwn_bufferDatafv (UpdwnBufferTarget target, uint length, const float *data, UpdwnBufferDataStoreUsage usage);
void updwn_bufferDataubv (UpdwnBufferTarget target, uint length, const uint8_t *data, UpdwnBufferDataStoreUsage usage);
void updwn_clearBufferDataubv (UpdwnBufferTarget target, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_clearNamedBufferDataubv (uint buffer, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_clearTexImageubv (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_clearTexSubImageubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data);
void updwn_compileShader (uint shader);
void updwn_createBuffers (int n, int *length, uint **buffers);
uint updwn_createProgram ();
uint updwn_createShader (UpdwnShaderType shaderType);
void updwn_createTextures (UpdwnTextureTarget target, int n, int *length, uint **textures);
void updwn_createVertexArrays (int n, int *length, uint **arrays);
void updwn_deleteBuffers (int n, const uint *buffers);
void updwn_deleteProgram (uint program);
void updwn_deleteShader (uint shader);
void updwn_deleteVertexArrays (int n, const uint *arrays);
void updwn_depthRangeArrayv (uint first, int length, const double *v);
void updwn_depthRangeIndexed (uint index, double nearVal, double farVal);
void updwn_detachShader (uint program, uint shader);
void updwn_disableVertexAttribArray (uint index);
void updwn_enableVertexAttribArray (uint index);
void updwn_genBuffers (int n, int *length, uint **buffers);
void updwn_genVertexArrays (int n, int *length, uint **arrays);
void updwn_getAttachedShaders (uint program, int maxCount, int *count, uint **shaders);
int  updwn_getAttribLocation (uint program, const char *name);
void updwn_getCompressedTextureSubImageubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, int *length, uint8_t **pixels);
void updwn_getProgramBinaryubv (uint program, int bufSize, int *length, uint *binaryFormat, uint8_t **binary);
void updwn_getProgramInfoLog (uint program, int maxLength, int *length, char **infoLog);
void updwn_getProgramiv (uint program, UpdwnProgramParam pname, int *length, int **params);
void updwn_getShaderInfoLog (uint shader, int maxLength, int *length, char **infoLog);
void updwn_getShaderiv (uint shader, UpdwnShaderParam pname, int *length, int **params);
void updwn_getShaderSource (uint shader, int bufSize, int *length, char **source);
void updwn_getTextureSubImageubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, int *length, uint8_t **pixels);
int  updwn_getUniformLocation (uint program, const char *name);
void updwn_getVertexArrayIndexed64iv (uint vaobj, uint index, UpdwnVertexAttribParam pname, int *length, int64_t **param);
void updwn_getVertexArrayIndexediv (uint vaobj, uint index, UpdwnVertexAttribParam pname, int *length, int **param);
void updwn_getVertexArrayiv (uint vaobj, UpdwnVertexArrayParam pname, int *length, int **param);
void updwn_getVertexAttribdv (uint index, UpdwnVertexAttribParam pname, int *length, double **params);
void updwn_getVertexAttribfv (uint index, UpdwnVertexAttribParam pname, int *length, float **params);
void updwn_getVertexAttribiv (uint index, UpdwnVertexAttribParam pname, int *length, int **params);
void updwn_getVertexAttribIiv (uint index, UpdwnVertexAttribParam pname, int *length, int **params);
void updwn_getVertexAttribIuiv (uint index, UpdwnVertexAttribParam pname, int *length, uint **params);
void updwn_getVertexAttribLdv (uint index, UpdwnVertexAttribParam pname, int *length, double **params);
void updwn_getVertexAttribPointerv (uint index, UpdwnVertexAttribParam pname, int *length, void **pointer);
bool updwn_isBuffer (uint buffer);
bool updwn_isProgram (uint program);
bool updwn_isShader (uint shader);
bool updwn_isVertexArray (uint array);
void updwn_linkProgram (uint program);
void updwn_namedBufferDatafv (uint buffer, uint length, const float *data, UpdwnBufferDataStoreUsage usage);
void updwn_namedBufferDataubv (uint buffer, uint length, const uint8_t *data, UpdwnBufferDataStoreUsage usage);
void updwn_objectLabel (UpdwnObjectType identifier, uint name, int length, const char *label);
void updwn_programBinaryubv (uint program, uint binaryFormat, const uint8_t *binary, int length);
void updwn_programParameteri (uint program, UpdwnProgramParam pname, int value);
void updwn_shaderBinaryubv (int count, const uint *shaders, uint binaryFormat, const uint8_t *binary, int length);
void updwn_shaderSource (uint shader, int count, const char **strings, const int *lengths);
void updwn_scissorArrayv (uint first, int length, const int *v);
void updwn_scissorIndexed (uint index, int left, int bottom, int width, int height);
void updwn_scissorIndexedv (uint index, const int *v);
void updwn_stencilFuncSeparate (UpdwnFace face, UpdwnComparison func, int ref, uint mask);
void updwn_stencilMaskSeparate (UpdwnFace face, uint mask);
void updwn_stencilOpSeparate (UpdwnFace face, UpdwnStencilAction sfail, UpdwnStencilAction dpfail, UpdwnStencilAction dppass);
void updwn_texImage2DMultisample (UpdwnTextureTarget target, int samples, UpdwnPixelInternalFormat internalformat, int width, int height, bool fixedsamplelocations);
void updwn_texImage3DMultisample (UpdwnTextureTarget target, int samples, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, bool fixedsamplelocations);
void updwn_uniform1f (int location, float v0);
void updwn_uniform2f (int location, float v0, float v1);
void updwn_uniform3f (int location, float v0, float v1, float v2);
void updwn_uniform4f (int location, float v0, float v1, float v2, float v3);
void updwn_uniform1i (int location, int v0);
void updwn_uniform2i (int location, int v0, int v1);
void updwn_uniform3i (int location, int v0, int v1, int v2);
void updwn_uniform4i (int location, int v0, int v1, int v2, int v3);
void updwn_uniform1ui (int location, uint v0);
void updwn_uniform2ui (int location, uint v0, uint v1);
void updwn_uniform3ui (int location, uint v0, uint v1, uint v2);
void updwn_uniform4ui (int location, uint v0, uint v1, uint v2, uint v3);
void updwn_uniform1fv (int location, int length, const float *value);
void updwn_uniform2fv (int location, int length, const float *value);
void updwn_uniform3fv (int location, int length, const float *value);
void updwn_uniform4fv (int location, int length, const float *value);
void updwn_uniform1iv (int location, int length, const int *value);
void updwn_uniform2iv (int location, int length, const int *value);
void updwn_uniform3iv (int location, int length, const int *value);
void updwn_uniform4iv (int location, int length, const int *value);
void updwn_uniform1uiv (int location, int length, const uint *value);
void updwn_uniform2uiv (int location, int length, const uint *value);
void updwn_uniform3uiv (int location, int length, const uint *value);
void updwn_uniform4uiv (int location, int length, const uint *value);
void updwn_uniformMatrix2fv (int location, bool transpose, int length, const float *value);
void updwn_uniformMatrix3fv (int location, bool transpose, int length, const float *value);
void updwn_uniformMatrix4fv (int location, bool transpose, int length, const float *value);
void updwn_uniformMatrix2x3fv (int location, bool transpose, int length, const float *value);
void updwn_uniformMatrix3x2fv (int location, bool transpose, int length, const float *value);
void updwn_uniformMatrix2x4fv (int location, bool transpose, int length, const float *value);
void updwn_uniformMatrix4x2fv (int location, bool transpose, int length, const float *value);
void updwn_uniformMatrix3x4fv (int location, bool transpose, int length, const float *value);
void updwn_uniformMatrix4x3fv (int location, bool transpose, int length, const float *value);
void updwn_useProgram (uint program);
void updwn_validateProgram(uint program);
void updwn_vertexArrayAttribBinding (uint vaobj, uint attribindex, uint bindingindex);
void updwn_vertexArrayAttribFormat (uint vaobj, uint attribindex, int size, UpdwnDataType type, bool normalized, uint relativeoffset);
void updwn_vertexArrayAttribIFormat (uint vaobj, uint attribindex, int size, UpdwnDataType type, uint relativeoffset);
void updwn_vertexArrayAttribLFormat (uint vaobj, uint attribindex, int size, UpdwnDataType type, uint relativeoffset);
void updwn_vertexArrayBindingDivisor (uint vaobj, uint bindingindex, uint divisor);
void updwn_vertexArrayVertexBuffer (uint vaobj, uint bindingindex, uint buffer, signed long offset, int stride);
void updwn_vertexArrayVertexBuffers (uint vaobj, uint first, int count, const uint *buffers, const signed long *offsets, const int *strides);
void updwn_vertexAttrib1d (uint index, double v0);
void updwn_vertexAttrib1f (uint index, float v0);
void updwn_vertexAttrib1s (uint index, int16_t v0);
void updwn_vertexAttrib2d (uint index, double v0, double v1);
void updwn_vertexAttrib2f (uint index, float v0, float v1);
void updwn_vertexAttrib2s (uint index, int16_t v0, int16_t v1);
void updwn_vertexAttrib3d (uint index, double v0, double v1, double v2);
void updwn_vertexAttrib3f (uint index, float v0, float v1, float v2);
void updwn_vertexAttrib3s (uint index, int16_t v0, int16_t v1, int16_t v2);
void updwn_vertexAttrib4d (uint index, double v0, double v1, double v2, double v3);
void updwn_vertexAttrib4f (uint index, float v0, float v1, float v2, float v3);
void updwn_vertexAttrib4s (uint index, int16_t v0, int16_t v1, int16_t v2, int16_t v3);
void updwn_vertexAttrib4Nub (uint index, uint8_t v0, uint8_t v1, uint8_t v2, uint8_t v3);
void updwn_vertexAttribI1i (uint index, int v0);
void updwn_vertexAttribI1ui (uint index, uint v0);
void updwn_vertexAttribI2i (uint index, int v0, int v1);
void updwn_vertexAttribI2ui (uint index, uint v0, uint v1);
void updwn_vertexAttribI3i (uint index, int v0, int v1, int v2);
void updwn_vertexAttribI3ui (uint index, uint v0, uint v1, uint v2);
void updwn_vertexAttribI4i (uint index, int v0, int v1, int v2, int v3);
void updwn_vertexAttribI4ui (uint index, uint v0, uint v1, uint v2, uint v3);
void updwn_vertexAttribL1d (uint index, double v0);
void updwn_vertexAttribL2d (uint index, double v0, double v1);
void updwn_vertexAttribL3d (uint index, double v0, double v1, double v2);
void updwn_vertexAttribL4d (uint index, double v0, double v1, double v2, double v3);
void updwn_vertexAttrib1dv (uint index, const double *v);
void updwn_vertexAttrib1fv (uint index, const float *v);
void updwn_vertexAttrib1sv (uint index, const int16_t *v);
void updwn_vertexAttrib2dv (uint index, const double *v);
void updwn_vertexAttrib2fv (uint index, const float *v);
void updwn_vertexAttrib2sv (uint index, const int16_t *v);
void updwn_vertexAttrib3dv (uint index, const double *v);
void updwn_vertexAttrib3fv (uint index, const float *v);
void updwn_vertexAttrib3sv (uint index, const int16_t *v);
void updwn_vertexAttrib4bv (uint index, const int8_t *v);
void updwn_vertexAttrib4dv (uint index, const double *v);
void updwn_vertexAttrib4fv (uint index, const float *v);
void updwn_vertexAttrib4iv (uint index, const int *v);
void updwn_vertexAttrib4sv (uint index, const int16_t *v);
void updwn_vertexAttrib4ubv (uint index, const uint8_t *v);
void updwn_vertexAttrib4uiv (uint index, const uint *v);
void updwn_vertexAttrib4usv (uint index, const uint16_t *v);
void updwn_vertexAttrib4Nbv (uint index, const int8_t *v);
void updwn_vertexAttrib4Niv (uint index, const int *v);
void updwn_vertexAttrib4Nsv (uint index, const int16_t *v);
void updwn_vertexAttrib4Nubv (uint index, const uint8_t *v);
void updwn_vertexAttrib4Nuiv (uint index, const uint *v);
void updwn_vertexAttrib4Nusv (uint index, const uint16_t *v);
void updwn_vertexAttribI1iv (uint index, const int *v);
void updwn_vertexAttribI1uiv (uint index, const uint *v);
void updwn_vertexAttribI2iv (uint index, const int *v);
void updwn_vertexAttribI2uiv (uint index, const uint *v);
void updwn_vertexAttribI3iv (uint index, const int *v);
void updwn_vertexAttribI3uiv (uint index, const uint *v);
void updwn_vertexAttribI4bv (uint index, const int8_t *v);
void updwn_vertexAttribI4iv (uint index, const int *v);
void updwn_vertexAttribI4sv (uint index, const int16_t *v);
void updwn_vertexAttribI4ubv (uint index, const uint8_t *v);
void updwn_vertexAttribI4uiv (uint index, const uint *v);
void updwn_vertexAttribI4usv (uint index, const uint16_t *v);
void updwn_vertexAttribL1dv (uint index, const double *v);
void updwn_vertexAttribL2dv (uint index, const double *v);
void updwn_vertexAttribL3dv (uint index, const double *v);
void updwn_vertexAttribL4dv (uint index, const double *v);
void updwn_vertexAttribP1ui (uint index, UpdwnDataType type, bool normalized, uint value);
void updwn_vertexAttribP2ui (uint index, UpdwnDataType type, bool normalized, uint value);
void updwn_vertexAttribP3ui (uint index, UpdwnDataType type, bool normalized, uint value);
void updwn_vertexAttribP4ui (uint index, UpdwnDataType type, bool normalized, uint value);
void updwn_vertexAttribBinding (uint attribindex, uint bindingindex);
void updwn_vertexAttribFormat (uint attribindex, int size, UpdwnDataType type, bool normalized, uint relativeoffset);
void updwn_vertexAttribIFormat (uint attribindex, int size, UpdwnDataType type, uint relativeoffset);
void updwn_vertexAttribIPointer (uint index, int size, UpdwnDataType type, int stride, const void *pointer);
void updwn_vertexAttribLFormat (uint attribindex, int size, UpdwnDataType type, uint relativeoffset);
void updwn_vertexAttribLPointer (uint index, int size, UpdwnDataType type, int stride, const void *pointer);
void updwn_vertexAttribPointer (uint index, int size, UpdwnDataType type, bool normalized, int stride, const void *pointer);
void updwn_vertexBindingDivisor (uint bindingindex, uint divisor);
void updwn_viewportArrayv (uint first, int length, const float *v);
void updwn_viewportIndexedf (uint index, float x, float y, float width, float height);
void updwn_viewportIndexedfv (uint index, const float *v);

#endif
