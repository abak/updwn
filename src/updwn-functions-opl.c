/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Documentation:
 *
 * Copyright 2003-2005 3Dlabs Inc. Ltd.
 * Copyright 2005 Addison-Wesley
 * Copyright 2010-2014 Khronos Group
 *
 * The documentation is licensed under the Open Publication License, v 1.0, 8 June 1999.
 * For details, see <https://opencontent.org/openpub/>.
 *
 * The original references are available on <https://github.com/KhronosGroup/OpenGL-Refpages>.
 * Modifications have been applied to fit the API to language bindings.
 */

#include "updwn-functions-opl.h"
#include "updwn-functions-utils.h"

/* use malloc */
#include <stdlib.h>

/* use printf (debug) */
//#include <stdio.h>

/**
 * SECTION: updwn-functions-opl
 * @section_id: updwn-functions-opl
 * @title: Functions - OPL
 * @short_description: Well-known OpenGL functions whose documentation is licensed under the OPL
 * @see_also: <link linkend="updwn-functions-sgi">Functions - SGI</link>
 *
 * &OPLThreeDlabsAddisonWesleyKhronosGroup;
 *
 * Full OpenGL documentation on the [Khronos website](https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/).
 */

/**
 * updwn_attachShader:
 * @program: the program object to which a shader object will be attached
 * @shader: the shader object that is to be attached
 *
 * Attach a shader object to a program object.
 */
void
updwn_attachShader (uint program, uint shader)
{
  glAttachShader (program, shader);
}

/**
 * updwn_bindAttribLocation:
 * @program: the handle of the program object in which the association is to be made
 * @index: the index of the generic vertex attribute to be bound
 * @name: a null terminated string containing the name of the vertex shader attribute variable to which @index is to be bound
 *
 * Associate a generic vertex attribute index with a named attribute variable.
 */
void
updwn_bindAttribLocation (uint program, uint index, const char *name)
{
  glBindAttribLocation (program, index, name);
}

/**
 * updwn_bindBuffer:
 * @target: the target to which the buffer object is bound
 * @buffer: the name of a buffer object
 *
 * Bind a named buffer object.
 */
void
updwn_bindBuffer (UpdwnBufferTarget target, uint buffer)
{
  glBindBuffer (target, buffer);
}

/**
 * updwn_bindBufferBase:
 * @target: the target of the bind operation
 * @index: the index of the binding point within the array specified by @target
 * @buffer: the name of a buffer object to bind to the specified binding point
 *
 * Bind a buffer object to an indexed buffer target.
 */
void
updwn_bindBufferBase (UpdwnBufferTarget target, uint index, uint buffer)
{
  glBindBufferBase (target, index, buffer);
}

/**
 * updwn_bindBufferRange:
 * @target: the target of the bind operation
 * @index: the index of the binding point within the array specified by @target
 * @buffer: the name of a buffer object to bind to the specified binding point
 * @offset: the starting offset in bytes into the buffer object @buffer
 * @size: the amount of data in bytes that can be read from the buffer object while used as an indexed target
 *
 * Bind a range within a buffer object to an indexed buffer target.
 */
void
updwn_bindBufferRange (UpdwnBufferTarget target, uint index, uint buffer, signed long offset, signed long size)
{
  glBindBufferRange (target, index, buffer, offset, size);
}

/**
 * updwn_bindBuffersBase:
 * @target: the target of the bind operation
 * @first: the index of the first binding point within the array specified by @target
 * @count: the number of contiguous binding points to which to bind buffers
 * @buffers: (nullable) (array length=count): a pointer to an array of names of buffer objects to bind to the targets on the specified binding point, or %NULL
 *
 * Bind one or more buffer objects to a sequence of indexed buffer targets.
 */
/* XXX:
 * The OpenGL doc mentions that glBindBuffersBase can be used to unbind buffers.
 * Test whether `Updwn.bindBuffersBase(target, first, [0, 0, 0])`
 * is equivalent to `glBindBuffersBase(target, first, 3, NULL)`.
 * If the test fails, @count shall be exposed in the introspection API
 * and another @length parameter introduced to hold @buffers length.
 */
void
updwn_bindBuffersBase (UpdwnBufferTarget target, uint first, int count, const uint *buffers)
{
  glBindBuffersBase (target, first, count, buffers);
}

/**
 * updwn_bindBuffersRange:
 * @target: the target of the bind operation
 * @first: the index of the first binding point within the array specified by @target
 * @count: the number of contiguous binding points to which to bind buffers
 * @buffers: (nullable) (array length=count): a pointer to an array of names of buffer objects to bind to the targets on the specified binding point, or %NULL
 * @offsets: (nullable) (array): a pointer to an array of offsets (in bytes) into the corresponding buffer in buffers to bind, or %NULL if buffers is %NULL
 * @sizes: (nullable) (array): a pointer to an array of sizes (in bytes) of the corresponding buffer in buffers to bind, or %NULL if buffers is %NULL
 *
 * Bind ranges of one or more buffer objects to a sequence of indexed buffer targets.
 */
/* XXX: same as for bindBuffersBase */
void
updwn_bindBuffersRange (UpdwnBufferTarget target, uint first, int count, const uint *buffers, const signed long *offsets, const signed long *sizes)
{
  glBindBuffersRange (target, first, count, buffers, offsets, sizes);
}

/**
 * updwn_bindTextures:
 * @first: the first texture unit to which a texture is to be bound
 * @count: the number of textures to bind
 * @textures: (array length=count): the address of an array of names of existing texture objects
 *
 * Bind one or more named textures to a sequence of consecutive texture units.
 */
void
updwn_bindTextures (UpdwnTextureTarget first, int count, const uint *textures)
{
  glBindTextures (first, count, textures);
}

/**
 * updwn_bindVertexArray:
 * @array: the name of the vertex array to bind
 *
 * Bind a vertex array object.
 */
void
updwn_bindVertexArray (uint array)
{
  glBindVertexArray (array);
}

/**
 * updwn_bindVertexBuffer:
 * @bindingindex: the index of the vertex buffer binding point to which to bind the buffer
 * @buffer: the name of a buffer to bind to the vertex buffer binding point
 * @offset: the offset in bytes of the first element of the buffer
 * @stride: the distance in bytes between elements within the buffer
 *
 * Bind a buffer to a vertex buffer bind point.
 *
 * The vertex array object is the currently bound vertex array object. Use updwn_vertexArrayVertexBuffer() variant to specify the vertex array object.
 */
void
updwn_bindVertexBuffer (uint bindingindex, uint buffer, signed long offset, int stride)
{
  glBindVertexBuffer (bindingindex, buffer, offset, stride);
}

/**
 * updwn_bindVertexBuffers:
 * @first: the first vertex buffer binding point to which a buffer object is to be bound
 * @count: the number of buffers to bind
 * @buffers: (array length=count): the address of an array of names of existing buffer objects
 * @offsets: (array): the address of an array of offsets (in bytes) to associate with the binding points
 * @strides: (array): the address of an array of strides (in bytes) to associate with the binding points
 *
 * Attach multiple buffer objects to a vertex array object.
 *
 * The vertex array object is the currently bound vertex array object. Use updwn_vertexArrayVertexBuffer() variant to specify the vertex array object.
 */
/* XXX: same as for bindBuffersBase */
void
updwn_bindVertexBuffers (uint first, int count, const uint *buffers, const signed long *offsets, const int *strides)
{
  glBindVertexBuffers (first, count, buffers, offsets, strides);
}

/**
 * updwn_blendEquationSeparate:
 * @modeRGB: how the red, green, and blue components of the source and destination colors are combined
 * @modeAlpha: how the alpha component of the source and destination colors are combined
 *
 * Set the RGB blend equation and the alpha blend equation separately, for all draw buffers.
 */
void
updwn_blendEquationSeparate (UpdwnBlendEquation modeRGB, UpdwnBlendEquation modeAlpha)
{
  glBlendEquationSeparate (modeRGB, modeAlpha);
}

/**
 * updwn_blendEquationSeparatei:
 * @buf: the index of the draw buffer for which to set the blend equations
 * @modeRGB: how the red, green, and blue components of the source and destination colors are combined
 * @modeAlpha: how the alpha component of the source and destination colors are combined
 *
 * Set the RGB blend equation and the alpha blend equation separately, for a single draw buffer.
 */
void
updwn_blendEquationSeparatei (uint buf, UpdwnBlendEquation modeRGB, UpdwnBlendEquation modeAlpha)
{
  glBlendEquationSeparatei (buf, modeRGB, modeAlpha);
}

/*
 * A bunch of bufferData<type>v functions is provided as convenient
 * alternatives to bufferDataBytes.
 */

#define BUFFER_DATA_TYPEV(updwn_func, data_type) \
void \
updwn_func (UpdwnBufferTarget target, uint length, const data_type *data, UpdwnBufferDataStoreUsage usage) \
{ \
  glBufferData (target, length * sizeof (data_type), data, usage); \
}

/**
 * updwn_bufferDatafv:
 * @target: the target to which the buffer object is bound
 * @length: length of @data
 * @data: (nullable) (array length=length): an array that will be copied into the data store for initialization, or %NULL if no data is to be copied
 * @usage: the expected usage pattern of the data store
 *
 * Create and initialize a buffer object's data store.
 *
 * <note>Behaves exactly like the original <function>glBufferData ()</function> function, but takes an array of floats.</note>
 */
BUFFER_DATA_TYPEV (updwn_bufferDatafv, float)

/**
 * updwn_bufferDataubv:
 * @target: the target to which the buffer object is bound
 * @length: length of @data
 * @data: (nullable) (array length=length): an array that will be copied into the data store for initialization, or %NULL if no data is to be copied
 * @usage: the expected usage pattern of the data store
 *
 * Create and initialize a buffer object's data store.
 *
 * <note>Behaves exactly like the original <function>glBufferData ()</function> function, but takes an array of unsigned bytes.</note>
 */
BUFFER_DATA_TYPEV (updwn_bufferDataubv, uint8_t)

#undef BUFFER_DATA_TYPEV

/**
 * updwn_clearBufferDataubv:
 * @target: the target to which the buffer object is bound
 * @internalformat: the internal format with which the data will be stored in the buffer object
 * @format: the format of the data contained in @data
 * @type: the type of the data contained in @data
 * @data: (nullable) (array): an array containing the data to be replicated into the buffer's data store, or %NULL to fill with zeros
 *
 * Fill a buffer object's data store with a fixed value.
 *
 * <note>Behaves exactly like the original <function>glClearBufferData ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_clearBufferDataubv (UpdwnBufferTarget target, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glClearBufferData (target, internalformat, format, type, data);
}

/**
 * updwn_clearNamedBufferDataubv:
 * @buffer: the name of the buffer object
 * @internalformat: the internal format with which the data will be stored in the buffer object
 * @format: the format of the data contained in @data
 * @type: the type of the data contained in @data
 * @data: (nullable) (array): an array containing the data to be replicated into the buffer's data store, or %NULL to fill with zeros
 *
 * Fill a buffer object's data store with a fixed value.
 *
 * <note>Behaves exactly like the original <function>glClearNamedBufferData ()</function> function, but takes an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_clearNamedBufferDataubv (uint buffer, UpdwnPixelInternalFormat internalformat, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glClearNamedBufferData (buffer, internalformat, format, type, data);
}

/**
 * updwn_clearTexImageubv:
 * @texture: the name of an existing texture object containing the image to be cleared
 * @level: the level of @texture containing the region to be cleared
 * @format: the format of the data contained in @data
 * @type: the type of the data contained in @data
 * @data: (nullable) (array): an array of between one and four components of texel data that will be used to clear the specified region, or %NULL to fill with zeros
 *
 * Fills all a texture image with a constant value.
 *
 * <note>Behaves exactly like the original <function>glClearTexImage ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_clearTexImageubv (uint texture, int level, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glClearTexImage (texture, level, format, type, data);
}

/**
 * updwn_clearTexSubImageubv:
 * @texture: the name of an existing texture object containing the image to be cleared
 * @level: the level of @texture containing the region to be cleared
 * @xoffset: the coordinate of the left edge of the region to be cleared
 * @yoffset: the coordinate of the lower edge of the region to be cleared
 * @zoffset: the coordinate of the front of the region to be cleared
 * @width: the width of the region to be cleared
 * @height: the height of the region to be cleared
 * @depth: the depth of the region to be cleared
 * @format: the format of the data whose address in memory is given by @data
 * @type: the type of the data whose address in memory is given by @data
 * @data: (nullable) (array): an array of between one and four components of texel data that will be used to clear the specified region, or %NULL to fill with zeros
 *
 * Fill all or part of a texture image with a constant value.
 *
 * <note>Behaves exactly like the original <function>glClearTexSubImage ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_clearTexSubImageubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, const uint8_t *data)
{
  glClearTexSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, data);
}

/**
 * updwn_compileShader:
 * @shader: the shader object to be compiled
 *
 * Compile a shader object.
 */
void
updwn_compileShader (uint shader)
{
  glCompileShader (shader);
}

/**
 * updwn_createBuffers:
 * @n: the number of buffer objects to create
 * @length: (out): location to store the length of @buffers
 * @buffers: (out) (array length=length) (element-type uint): an array in which names of the new buffer objects are stored, use free() to free the returned array
 *
 * Create buffer object.
 *
 * OpenGL: 4.5
 */
void
updwn_createBuffers (int n, int *length, uint **buffers)
{
  if (*buffers == NULL)
    {
      *length = n;
      *buffers = malloc (sizeof (uint) * n);
    }

  glCreateBuffers (n, *buffers);
}

/**
 * updwn_createProgram:
 *
 * Create a program object.
 *
 * Returns: a non-zero value by which it can be referenced, or 0 if an error occurs creating the shader object
 */
uint
updwn_createProgram ()
{
  return glCreateProgram ();
}

/**
 * updwn_createShader:
 * @shaderType: the type of shader to be created
 *
 * Create a shader object.
 *
 * Returns: a non-zero value by which it can be referenced, or 0 if an error occurs creating the shader object
 */
uint
updwn_createShader (UpdwnShaderType shaderType)
{
  return glCreateShader (shaderType);
}

/**
 * updwn_createTextures:
 * @target: the effective texture target of each created texture
 * @n: the number of texture objects to create
 * @length: (out): location to store the length of @textures
 * @textures: (out) (array length=length) (element-type uint): an array in which names of the new texture objects are stored, use free() to free the returned array
 *
 * Create texture objects.
 *
 * OpenGL: 4.5
 */
void
updwn_createTextures (UpdwnTextureTarget target, int n, int *length, uint **textures)
{
  if (*textures == NULL)
    {
      *length = n;
      *textures = malloc (sizeof (uint) * n);
    }

  glCreateTextures (target, n, *textures);
}

/**
 * updwn_createVertexArrays:
 * @n: the number of vertex array objects to create
 * @length: (out): location to store the length of @arrays
 * @arrays: (out) (array length=length) (element-type uint): an array in which names of the new vertex array objects are stored, use free() to free the returned array
 *
 * Create vertex array objects.
 *
 * OpenGL: 4.5
 */
void
updwn_createVertexArrays (int n, int *length, uint **arrays)
{
  if (*arrays == NULL)
    {
      *length = n;
      *arrays = malloc (sizeof (uint) * n);
    }

  glCreateVertexArrays (n, *arrays);
}

/**
 * updwn_deleteBuffers:
 * @n: the number of buffer objects to be deleted
 * @buffers: (array length=n) (element-type uint): an array of buffer objects to be deleted
 *
 * Delete named buffer objects.
 */
void
updwn_deleteBuffers (int n, const uint *buffers)
{
  glDeleteBuffers (n, buffers);
}

/**
 * updwn_deleteProgram:
 * @program: the program object to be deleted
 *
 * Delete a program object.
 */
void
updwn_deleteProgram (uint program)
{
  glDeleteProgram (program);
}

/**
 * updwn_deleteShader:
 * @shader: the shader object to be deleted
 *
 * Delete a shader object.
 */
void
updwn_deleteShader (uint shader)
{
  glDeleteShader (shader);
}

/**
 * updwn_deleteVertexArrays:
 * @n: the number of vertex array objects to be deleted
 * @arrays: (array length=n) (element-type uint): an array containing the n names of the objects to be deleted
 *
 * Delete vertex array objects.
 */
void
updwn_deleteVertexArrays (int n, const uint *arrays)
{
  glDeleteVertexArrays (n, arrays);
}

/**
 * updwn_depthRangeArrayv:
 * @first: the index of the first viewport whose depth range to update
 * @length: the length of @v
 * @v: (array length=length): an array containing the near and far values for the depth range of each modified viewport
 *
 * Specify mapping of depth values from normalized device coordinates to window coordinates for a specified set of viewports.
 *
 * <note>@length must be equal to `2 x count` where `count` is the number of viewports whose depth range to update.</note>
 */
void
updwn_depthRangeArrayv (uint first, int length, const double *v)
{
  int count = length / 2;
  glDepthRangeArrayv (first, count, v);
}

/**
 * updwn_depthRangeIndexed:
 * @index: the index of the viewport whose depth range to update
 * @nearVal: the mapping of the near clipping plane to window coordinates
 * @farVal: the mapping of the far clipping plane to window coordinates
 *
 * Specify mapping of depth values from normalized device coordinates to window coordinates for a specified viewport.
 */
void
updwn_depthRangeIndexed (uint index, double nearVal, double farVal)
{
  glDepthRangeIndexed (index, nearVal, farVal);
}

/**
 * updwn_detachShader:
 * @program: the program object from which to detach the shader object
 * @shader: the shader object to be detached
 *
 * Detache a shader object from a program object to which it is attached.
 */
void
updwn_detachShader (uint program, uint shader)
{
  glDetachShader (program, shader);
}

/**
 * updwn_disableVertexAttribArray:
 * @index: the index of the generic vertex attribute to be disabled
 *
 * Disable a generic vertex attribute array.
 */
void
updwn_disableVertexAttribArray (uint index)
{
  glDisableVertexAttribArray (index);
}

/**
 * updwn_enableVertexAttribArray:
 * @index: the index of the generic vertex attribute to be enabled
 *
 * Enable a generic vertex attribute array.
 */
void
updwn_enableVertexAttribArray (uint index)
{
  glEnableVertexAttribArray (index);
}

/**
 * updwn_genBuffers:
 * @n: the number of buffer object names to be generated
 * @length: (out): location to store the length of @buffers
 * @buffers: (out) (array length=length) (element-type uint): an array in which the generated buffer object names are stored, use free() to free the returned array
 *
 * Generate buffer object names.
 */
void
updwn_genBuffers (int n, int *length, uint **buffers)
{
  if (*buffers == NULL)
    {
      *length = n;
      *buffers = malloc (sizeof (uint) * n);
    }

  glGenBuffers (n, *buffers);
}

/**
 * updwn_genVertexArrays:
 * @n: the number of vao names to generate
 * @length: (out): location to store the length of @arrays
 * @arrays: (out) (array length=length) (element-type uint): an array in which the generated vao names are stored, use free() to free the returned array
 *
 * Generate vertex array object names.
 */
void
updwn_genVertexArrays (int n, int *length, uint **arrays)
{
  if (*arrays == NULL)
    {
      *length = n;
      *arrays = malloc (sizeof (uint) * n);
    }

  glGenVertexArrays (n, *arrays);
}

/**
 * updwn_getAttachedShaders:
 * @program: the program object to be queried
 * @maxCount: the size of the array for storing the returned object names, or -1
 * @count: (out): location to store the number of names actually returned in shaders
 * @shaders: (out) (array length=count) (element-type uint): an array that is used to return the names of attached shader objects, use free() to free the returned array
 *
 * Get the handles of the shader objects attached to a program object.
 */
void
updwn_getAttachedShaders (uint program, int maxCount, int *count, uint **shaders)
{
  if (*shaders == NULL)
    {
      if (maxCount < 0)
        glGetProgramiv (program, GL_ATTACHED_SHADERS, &maxCount);

      *shaders = malloc (sizeof (uint) * maxCount);
    }

  glGetAttachedShaders (program, maxCount, count, *shaders);
}

/**
 * updwn_getAttribLocation:
 * @program: the program object to be queried
 * @name: Points to a null terminated string containing the name of the attribute variable whose location is to be queried
 *
 * Get the location of an attribute variable.
 *
 * Returns: the index of the generic vertex attribute that is bound to that attribute variable, or -1.
 */
int
updwn_getAttribLocation (uint program, const char *name)
{
  return glGetAttribLocation (program, name);
}

/**
 * updwn_getCompressedTextureSubImageubv:
 * @texture: the name of the source texture object
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage, must be a multiple of the compressed block's width, unless the offset is zero and the size equals the texture image size
 * @height: the height of the texture subimage, must be a multiple of the compressed block's height, unless the offset is zero and the size equals the texture image size
 * @depth: the depth of the texture subimage, must be a multiple of the compressed block's depth, unless the offset is zero and the size equals the texture image size
 * @bufSize: the size of the buffer to receive the retrieved pixel data
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @pixels
 * @pixels: (out) (array length=length): the address of an array to return the texture subimage, use free() to free the returned array
 *
 * Retrieve a sub-region of a compressed texture image from a compressed texture object.
 *
 * <note>Behaves exactly like the original <function>glGetCompressedTextureSubImage ()</function> function, but returns an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getCompressedTextureSubImageubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, int bufSize, int *length, uint8_t **pixels)
{
  if (length != NULL)
    *length = bufSize;

  if (*pixels == NULL)
    *pixels = malloc (bufSize);

  glGetCompressedTextureSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, bufSize, *pixels);
}

/**
 * updwn_getProgramBinaryubv:
 * @program: the name of a program object whose binary representation to retrieve
 * @bufSize: the size of the buffer whose address is given by @binary, or `-1`
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @binary
 * @binaryFormat: (out): the address of a variable to receive a token indicating the format of the binary data returned by the GL
 * @binary: (out) (array length=length): the address of an array into which the GL will return @program's binary representation, use free() to free the returned array
 *
 * Get a binary representation of a program object's compiled and linked executable source.
 *
 * <note>Behaves exactly like the original <function>glGetProgramBinary ()</function> function, but returns an array of unsigned bytes.</note>
 */
void
updwn_getProgramBinaryubv (uint program, int bufSize, int *length, uint *binaryFormat, uint8_t **binary)
{
  if (*binary == NULL)
    {
      if (bufSize == -1)
        glGetProgramiv (program, GL_PROGRAM_BINARY_LENGTH, &bufSize);

      *binary = malloc (bufSize);
    }

  glGetProgramBinary (program, bufSize, length, binaryFormat, *binary);
}

/**
 * updwn_getProgramInfoLog:
 * @program: the program object whose information log is to be queried
 * @maxLength: the size of the character buffer for storing the returned information log, or -1
 * @length: (out): return the length of the string returned in @infoLog (excluding the null terminator)
 * @infoLog: (out) (array length=length) (element-type guint8): an array of characters that is used to return the information log, use free() to free the returned string
 *
 * Get the information log for a program object.
 */
void
updwn_getProgramInfoLog (uint program, int maxLength, int *length, char **infoLog)
{
  if (maxLength == -1)
    glGetProgramiv (program, GL_INFO_LOG_LENGTH, &maxLength);

  if (maxLength < 0)
    maxLength = 0;

  if (*infoLog == NULL)
    *infoLog = malloc (maxLength + 1);

  glGetProgramInfoLog (program, maxLength, length, *infoLog);
}

/**
 * updwn_getProgramiv:
 * @program: the program object to be queried
 * @pname: the object parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested object parameter, use free() to free the returned array
 *
 * Get a parameter from a program object.
 */
void
updwn_getProgramiv (uint program, UpdwnProgramParam pname, int *length, int **params)
{
  int count = updwn_get_program_param_count (pname);

  if (length != NULL)
    *length = count;

  if (*params == NULL)
    *params = malloc (count * sizeof (int));

  glGetProgramiv (program, pname, *params);
}

/**
 * updwn_getShaderInfoLog:
 * @shader: the shader object whose information log is to be queried
 * @maxLength: the size of the character buffer for storing the returned information log, or -1
 * @length: (out): return the length of the string returned in @infoLog (excluding the null terminator)
 * @infoLog: (out) (array length=length) (element-type guint8): an array of characters that is used to return the information log, use free() to free the returned string
 *
 * Get the information log for a shader object.
 */
void
updwn_getShaderInfoLog (uint shader, int maxLength, int *length, char **infoLog)
{
  if (maxLength == -1)
    glGetShaderiv (shader, GL_INFO_LOG_LENGTH, &maxLength);

  if (maxLength < 0)
    maxLength = 0;

  if (*infoLog == NULL)
    *infoLog = malloc (maxLength + 1);

  glGetShaderInfoLog (shader, maxLength, length, *infoLog);
}

/**
 * updwn_getShaderiv:
 * @shader: the shader object to be queried
 * @pname: the object parameter
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested object parameter, use free() to free the returned array
 *
 * Get a parameter from a shader object.
 */
void
updwn_getShaderiv (uint shader, UpdwnShaderParam pname, int *length, int **params)
{
  int count = updwn_get_shader_param_count (pname);

  if (length != NULL)
    *length = count;

  if (*params == NULL)
    *params = malloc (count * sizeof (int));

  glGetShaderiv (shader, pname, *params);
}

/**
 * updwn_getShaderSource:
 * @shader: the shader object to be queried
 * @bufSize: the size of the character buffer for storing the returned source code string, or -1
 * @length: (out): returns the length of the string returned in source (excluding the null terminator)
 * @source: (out) (array length=length) (element-type guint8): an array of characters that is used to return the source code string, use free() to free the returned string
 *
 * Get the information log for a shader object.
 */
void
updwn_getShaderSource (uint shader, int bufSize, int *length, char **source)
{
  if (bufSize == -1)
    glGetShaderiv (shader, GL_SHADER_SOURCE_LENGTH, &bufSize);

  if (bufSize < 0)
    bufSize = 0;

  if (*source == NULL)
    *source = malloc (bufSize + 1);

  glGetShaderSource (shader, bufSize, length, *source);
}

/**
 * updwn_getTextureSubImageubv:
 * @texture: the name of the source texture object
 * @level: the level-of-detail number of the desired image, `0` is the base image level, `n` is the nth mipmap reduction image
 * @xoffset: a texel offset in the x direction within the texture array
 * @yoffset: a texel offset in the y direction within the texture array
 * @zoffset: a texel offset in the z direction within the texture array
 * @width: the width of the texture subimage, must be a multiple of the compressed block's width, unless the offset is zero and the size equals the texture image size
 * @height: the height of the texture subimage, must be a multiple of the compressed block's height, unless the offset is zero and the size equals the texture image size
 * @depth: the depth of the texture subimage, must be a multiple of the compressed block's depth, unless the offset is zero and the size equals the texture image size
 * @format: the format of the pixel data
 * @type: the data type of the pixel data
 * @bufSize: the size of the buffer to receive the retrieved pixel data
 * @length: (out) (optional): the address of a variable to receive the number of bytes written into @pixels
 * @pixels: (out) (array length=length): the address of an array to return the texture subimage, use free() to free the returned array
 *
 * Retrieve a sub-region of a texture image from a texture object.
 *
 * <note>Behaves exactly like the original <function>glGetTextureSubImage ()</function> function, but returns an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
void
updwn_getTextureSubImageubv (uint texture, int level, int xoffset, int yoffset, int zoffset, int width, int height, int depth, UpdwnPixelFormat format, UpdwnDataType type, int bufSize, int *length, uint8_t **pixels)
{
  if (length != NULL)
    *length = bufSize;

  if (*pixels == NULL)
    *pixels = malloc (bufSize);

  glGetTextureSubImage (texture, level, xoffset, yoffset, zoffset, width, height, depth, format, type, bufSize, *pixels);
}

/**
 * updwn_getUniformLocation:
 * @program: the program object to be queried
 * @name: points to a null terminated string containing the name of the uniform variable whose location is to be queried
 *
 * Get the location of a uniform variable.
 *
 * Returns: an integer that represents the location of a specific uniform variable within a program object, or -1.
 */
int
updwn_getUniformLocation (uint program, const char *name)
{
  return glGetUniformLocation (program, name);
}

#define GET_VERTEX_ARRAY_INDEXED_TYPEV(updwn_func, data_type, gl_func) \
void \
updwn_func (uint vaobj, uint index, UpdwnVertexAttribParam pname, int *length, data_type **param) \
{ \
  int count = updwn_get_vertex_attrib_param_count (pname);\
  \
  if (length != NULL) \
    *length = count; \
  \
  if (*param == NULL) \
    *param = malloc (count * sizeof (data_type)); \
  \
  gl_func (vaobj, index, pname, *param); \
}

/**
 * updwn_getVertexArrayIndexed64iv:
 * @vaobj: the name of a vertex array object
 * @index: the index of the vertex array object attribute, must be a number between 0 and (`GL_MAX_VERTEX_ATTRIBS` - 1)
 * @pname: the property to be used for the query, must be `GL_VERTEX_BINDING_OFFSET`
 * @length: (out) (optional): return the length of @param, or %NULL
 * @param: (out) (array length=length): returns the requested value, use free() to free the returned array
 *
 * Retrieve parameters of an attribute of a vertex array object.
 *
 * OpenGL: 4.5
 */
GET_VERTEX_ARRAY_INDEXED_TYPEV (updwn_getVertexArrayIndexed64iv, int64_t, glGetVertexArrayIndexed64iv)

/**
 * updwn_getVertexArrayIndexediv:
 * @vaobj: the name of a vertex array object
 * @index: the index of the vertex array object attribute, must be a number between 0 and (`GL_MAX_VERTEX_ATTRIBS` - 1)
 * @pname: the property to be used for the query
 * @length: (out) (optional): return the length of @param, or %NULL
 * @param: (out) (array length=length): returns the requested value, use free() to free the returned array
 *
 * Retrieve parameters of an attribute of a vertex array object.
 *
 * OpenGL: 4.5
 */
GET_VERTEX_ARRAY_INDEXED_TYPEV (updwn_getVertexArrayIndexediv, int, glGetVertexArrayIndexediv)

#undef GET_VERTEX_ARRAY_INDEXED_TYPEV

/**
 * updwn_getVertexArrayiv:
 * @vaobj: the name of the vertex array object to use for the query
 * @pname: name of the property to use for the query, must be `GL_ELEMENT_ARRAY_BUFFER_BINDING`
 * @length: (out) (optional): return the length of @param, or %NULL
 * @param: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Retrieve parameters of a vertex array object.
 *
 * OpenGL: 4.5
 */
void
updwn_getVertexArrayiv (uint vaobj, UpdwnVertexArrayParam pname, int *length, int **param)
{
  int count = updwn_get_vertex_array_param_count (pname);

  if (length != NULL)
    *length = count;

  if (*param == NULL)
    *param = malloc (count * sizeof (int));

  glGetVertexArrayiv (vaobj, pname, *param);
}

#define GET_VERTEX_ATTRIB_TYPEV(updwn_func, data_type, gl_func) \
void \
updwn_func (uint index, UpdwnVertexAttribParam pname, int *length, data_type **params) \
{ \
  int count = updwn_get_vertex_attrib_param_count (pname); \
  \
  if (length != NULL) \
    *length = count; \
  \
  if (*params == NULL) \
    *params = malloc (count * sizeof (data_type)); \
  \
  gl_func (index, pname, *params); \
}

/**
 * updwn_getVertexAttribdv:
 * @index: the generic vertex attribute parameter to be queried
 * @pname: the symbolic name of the vertex attribute parameter to be queried
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Retrieve a generic vertex attribute parameter.
 */
GET_VERTEX_ATTRIB_TYPEV (updwn_getVertexAttribdv, double, glGetVertexAttribdv)

/**
 * updwn_getVertexAttribfv:
 * @index: the generic vertex attribute parameter to be queried
 * @pname: the symbolic name of the vertex attribute parameter to be queried
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Retrieve a generic vertex attribute parameter.
 */
GET_VERTEX_ATTRIB_TYPEV (updwn_getVertexAttribfv, float, glGetVertexAttribfv)

/**
 * updwn_getVertexAttribiv:
 * @index: the generic vertex attribute parameter to be queried
 * @pname: the symbolic name of the vertex attribute parameter to be queried
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Retrieve a generic vertex attribute parameter.
 */
GET_VERTEX_ATTRIB_TYPEV (updwn_getVertexAttribiv, int, glGetVertexAttribiv)

/**
 * updwn_getVertexAttribIiv:
 * @index: the generic vertex attribute parameter to be queried
 * @pname: the symbolic name of the vertex attribute parameter to be queried
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Retrieve a generic vertex attribute parameter.
 */
GET_VERTEX_ATTRIB_TYPEV (updwn_getVertexAttribIiv, int, glGetVertexAttribIiv)

/**
 * updwn_getVertexAttribIuiv:
 * @index: the generic vertex attribute parameter to be queried
 * @pname: the symbolic name of the vertex attribute parameter to be queried
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Retrieve a generic vertex attribute parameter.
 */
GET_VERTEX_ATTRIB_TYPEV (updwn_getVertexAttribIuiv, uint, glGetVertexAttribIuiv)

/**
 * updwn_getVertexAttribLdv:
 * @index: the generic vertex attribute parameter to be queried
 * @pname: the symbolic name of the vertex attribute parameter to be queried
 * @length: (out) (optional): return the length of @params, or %NULL
 * @params: (out) (array length=length): returns the requested data, use free() to free the returned array
 *
 * Retrieve a generic vertex attribute parameter.
 */
GET_VERTEX_ATTRIB_TYPEV (updwn_getVertexAttribLdv, double, glGetVertexAttribLdv)

#undef GET_VERTEX_ATTRIB_TYPEV

/**
 * updwn_getVertexAttribPointerv:
 * @index: the generic vertex attribute parameter to be returned
 * @pname: the symbolic name of the generic vertex attribute parameter to be returned, must be `GL_VERTEX_ATTRIB_ARRAY_POINTER`
 * @length: (out) (optional): return the length of @pointer, or %NULL
 * @pointer: (out) (array length=length) (element-type gint): returns the pointer (offset) value, use free() to free the returned array
 *
 * Retrieve the address of the specified generic vertex attribute pointer (offset).
 */
void
updwn_getVertexAttribPointerv (uint index, UpdwnVertexAttribParam pname, int *length, void **pointer)
{
  int count = updwn_get_vertex_attrib_param_count (pname);

  if (length != NULL)
    *length = count;

  if (*pointer == NULL)
    *pointer = malloc (count * sizeof (int));

  glGetVertexAttribPointerv (index, pname, *pointer);
}

/**
 * updwn_isBuffer:
 * @buffer: a value that may be the name of a buffer object
 *
 * Determine if a name corresponds to a buffer object.
 *
 * Notes: a name returned by updwn_genBuffers(), but not yet associated with a buffer object by calling updwn_bindBuffer(), is not the name of a buffer object.
 *
 * Returns: %TRUE if @buffer is currently the name of a buffer object, %FALSE otherwise
 */
bool
updwn_isBuffer (uint buffer)
{
  return glIsBuffer (buffer);
}

/**
 * updwn_isProgram:
 * @program: a potential program object
 *
 * Determine if a name corresponds to a program object.
 *
 * Notes: a program object marked for deletion with updwn_deleteProgram() but still in use as part of current rendering state is still considered a program object and updwn_isProgram() will return %TRUE.
 *
 * Returns: %TRUE if @program is the name of a program object previously created with updwn_createProgram() and not yet deleted with updwn_deleteProgram(), %FALSE otherwise
 */
bool
updwn_isProgram (uint program)
{
  return glIsProgram (program);
}

/**
 * updwn_isShader:
 * @shader: a potential shader object
 *
 * Determine if a name corresponds to a shader object.
 *
 * Notes: a shader object marked for deletion with updwn_deleteShader() but still attached to a program object is still considered a shader object and updwn_isShader() will return %TRUE.
 *
 * Returns: %TRUE if @shader is the name of a shader object previously created with updwn_createShader() and not yet deleted with updwn_deleteShader(), %FALSE otherwise
 */
bool
updwn_isShader (uint shader)
{
  return glIsShader (shader);
}

/**
 * updwn_isVertexArray:
 * @array: a value that may be the name of a vertex array object
 *
 * Determine if a name corresponds to a vertex array object.
 *
 * Notes: if @array is a name returned by updwn_genVertexArrays(), by that has not yet been bound through a call to updwn_bindVertexArray(), then the name is not a vertex array object and updwn_isVertexArray() returns %FALSE.
 *
 * Returns: %TRUE if @array is currently the name of a vertex array object, %FALSE otherwise
 */
bool
updwn_isVertexArray (uint array)
{
  return glIsVertexArray (array);
}

/**
 * updwn_linkProgram:
 * @program: the handle of the program object to be linked
 *
 * Link a program object.
 */
void
updwn_linkProgram (uint program)
{
  glLinkProgram (program);
}

/*
 * A bunch of namedBufferData<type>v functions is provided as convenient
 * alternatives to nameBufferDataBytes().
 */

#define NAMED_BUFFER_DATA_TYPEV(updwn_func, data_type) \
void \
updwn_func (uint buffer, uint length, const data_type *data, UpdwnBufferDataStoreUsage usage) \
{ \
  glNamedBufferData (buffer, length * sizeof (data_type), data, usage); \
}

/**
 * updwn_namedBufferDatafv:
 * @buffer: the name of the buffer object
 * @length: length of @data
 * @data: (nullable) (array length=length): an array that will be copied into the data store for initialization, or %NULL if no data is to be copied
 * @usage: the expected usage pattern of the data store
 *
 * Create and initialize a buffer object's data store.
 *
 * <note>Behaves exactly like the original <function>glNamedBufferData ()</function> function, but takes an array of floats.</note>
 *
 * OpenGL: 4.5
 */
NAMED_BUFFER_DATA_TYPEV (updwn_namedBufferDatafv, float)

/**
 * updwn_namedBufferDataubv:
 * @buffer: the name of the buffer object
 * @length: length of @data
 * @data: (nullable) (array length=length): an array that will be copied into the data store for initialization, or %NULL if no data is to be copied
 * @usage: the expected usage pattern of the data store
 *
 * Create and initialize a buffer object's data store.
 *
 * <note>Behaves exactly like the original <function>glNamedBufferData ()</function> function, but takes an array of unsigned bytes.</note>
 *
 * OpenGL: 4.5
 */
NAMED_BUFFER_DATA_TYPEV (updwn_namedBufferDataubv, uint8_t)

#undef NAMED_BUFFER_DATA_TYPEV

/**
 * updwn_objectLabel:
 * @identifier: the namespace from which the name of the object is allocated
 * @name: the name of the object to label
 * @length: the length of the label to be used for the object
 * @label: (array length=length) (element-type guint8): the address of a string containing the label to assign to the object
 *
 * Label a named object identified within a namespace.
 */
void
updwn_objectLabel (UpdwnObjectType identifier, uint name, int length, const char *label)
{
  glObjectLabel (identifier, name, length, label);
}

/**
 * updwn_programBinaryubv:
 * @program: the name of a program object into which to load a program binary
 * @binaryFormat: the format of the binary data in @binary
 * @binary: (nullable) (array length=length): the address of an array containing the binary to be loaded into @program, or %NULL if no binary is to be loaded
 * @length: the number of bytes contained in @binary
 *
 * Load a program object with a program binary.
 *
 * <note>Behaves exactly like the original <function>glProgramBinary ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_programBinaryubv(uint program, uint binaryFormat, const uint8_t *binary, int length)
{
  glProgramBinary (program, binaryFormat, binary, length);
}

/**
 * updwn_programParameteri:
 * @program: the name of a program object whose parameter to modify
 * @pname: the name of the parameter to modify
 * @value: the new value of the parameter specified by @pname for @program
 *
 * Specify a parameter for a program object.
 */
void
updwn_programParameteri (uint program, UpdwnProgramParam pname, int value)
{
  glProgramParameteri (program, pname, value);
}

/**
 * updwn_shaderBinaryubv:
 * @count: the number of shader object handles contained in @shaders
 * @shaders: (array length=count): the address of an array of shader handles into which to load pre-compiled shader binaries
 * @binaryFormat: the format of the shader binaries contained in @binary
 * @binary: (nullable) (array length=length): the address of an array of bytes containing pre-compiled binary shader code, or %NULL
 * @length: the length of the array whose address is given in @binary
 *
 * Load pre-compiled shader binaries.
 *
 * <note>Behaves exactly like the original <function>glShaderBinary ()</function> function, but takes an array of unsigned bytes.</note>
 */
void
updwn_shaderBinaryubv (int count, const uint *shaders, uint binaryFormat, const uint8_t *binary, int length)
{
  glShaderBinary (count, shaders, binaryFormat, binary, length);
}

/**
 * updwn_shaderSource:
 * @shader: the handle of the shader object whose source code is to be replaced
 * @count: the number of elements in the string and length arrays
 * @strings: (array length=count): an array of pointers to strings containing the source code to be loaded into the shader
 * @lengths: (nullable) (array) (element-type int): an array of string lengths, or %NULL if each string is null terminated
 *
 * Replace the source code in a shader object.
 */
void
updwn_shaderSource (uint shader, int count, const char **strings, const int *lengths)
{
  glShaderSource (shader, count, strings, lengths);
}

/**
 * updwn_scissorArrayv:
 * @first: the index of the first viewport whose scissor box to modify
 * @length: the length of v
 * @v: (array length=length): an array containing the left, bottom, width and height of each scissor box, in that order
 *
 * Define the scissor box for multiple viewports.
 *
 * <note>@length must be equal to `4 x count` where `count` is the number of scissor boxes to modify.</note>
 */
void
updwn_scissorArrayv (uint first, int length, const int *v)
{
  int count = length / 4;
  glScissorArrayv (first, count, v);
}

/**
 * updwn_scissorIndexed:
 * @index: the index of the viewport whose scissor box to modify
 * @left: the x coordinate of the lower left corner of the scissor box, in pixels
 * @bottom: the y coordinate of the lower left corner of the scissor box, in pixels
 * @width: the width of the scissor box, in pixels
 * @height: the height of the scissor box, in pixels
 *
 * Define the scissor box for a specific viewport.
 */
void
updwn_scissorIndexed (uint index, int left, int bottom, int width, int height)
{
  glScissorIndexed (index, left, bottom, width, height);
}

/**
 * updwn_scissorIndexedv:
 * @index: the index of the viewport whose scissor box to modify
 * @v: (array): an array containing the left, bottom, width and height of each scissor box, in that order
 *
 * Define the scissor box for a specific viewport.
 */
void
updwn_scissorIndexedv (uint index, const int *v)
{
  glScissorIndexedv (index, v);
}

/**
 * updwn_stencilFuncSeparate:
 * @face: whether front and/or back stencil state is updated
 * @func: the test function
 * @ref: the reference value for the stencil test, clamped to the range `[0, 2n - 1]` where n is the number of bitplanes in the stencil buffer
 * @mask: a mask that is ANDed with both the reference value and the stored stencil value when the test is done
 *
 * Set front and back function and reference value for stencil testing.
 */
void
updwn_stencilFuncSeparate (UpdwnFace face, UpdwnComparison func, int ref, uint mask)
{
  glStencilFuncSeparate (face, func, ref, mask);
}

/**
 * updwn_stencilMaskSeparate:
 * @face: whether the front and/or back stencil writemask is updated
 * @mask: a bit mask to enable and disable writing of individual bits in the stencil planes
 *
 * Control the front and/or back writing of individual bits in the stencil planes.
 */
void
updwn_stencilMaskSeparate (UpdwnFace face, uint mask)
{
  glStencilMaskSeparate (face, mask);
}

/**
 * updwn_stencilOpSeparate:
 * @face: whether front and/or back stencil state is updated
 * @sfail: the action to take when the stencil test fails
 * @dpfail: the stencil action when the stencil test passes, but the depth test fails
 * @dppass: the stencil action when both the stencil test and the depth test pass, or when the stencil test passes and either there is no depth buffer or depth testing is not enabled
 *
 * Set front and/or back stencil test actions.
 */
void
updwn_stencilOpSeparate (UpdwnFace face, UpdwnStencilAction sfail, UpdwnStencilAction dpfail, UpdwnStencilAction dppass)
{
  glStencilOpSeparate (face, sfail, dpfail, dppass);
}

/**
 * updwn_texImage2DMultisample:
 * @target: the target of the operation, must be `GL_TEXTURE_2D_MULTISAMPLE` or `GL_PROXY_TEXTURE_2D_MULTISAMPLE`
 * @samples: the number of samples in the multisample texture's image
 * @internalformat: the internal format to be used to store the multisample texture's image, must specify a color-renderable, depth-renderable, or stencil-renderable format
 * @width: the width of the multisample texture's image, in texels
 * @height: the height of the multisample texture's image, in texels
 * @fixedsamplelocations: whether the image will use identical sample locations and the same number of samples for all texels in the image, and the sample locations will not depend on the internal format or size of the image
 *
 * Establish the data storage, format, dimensions, and number of samples of a multisample texture's image.
 */
void
updwn_texImage2DMultisample (UpdwnTextureTarget target, int samples, UpdwnPixelInternalFormat internalformat, int width, int height, bool fixedsamplelocations)
{
  glTexImage2DMultisample (target, samples, internalformat, width, height, fixedsamplelocations);
}

/**
 * updwn_texImage3DMultisample:
 * @target: the target of the operation, must be `GL_TEXTURE_2D_MULTISAMPLE_ARRAY` or `GL_PROXY_TEXTURE_2D_MULTISAMPLE_ARRAY`
 * @samples: the number of samples in the multisample texture's image
 * @internalformat: the internal format to be used to store the multisample texture's image, must specify a color-renderable, depth-renderable, or stencil-renderable format
 * @width: the width of the multisample texture's image, in texels
 * @height: the height of the multisample texture's image, in texels
 * @depth: the number of array slices in the array texture's image
 * @fixedsamplelocations: whether the image will use identical sample locations and the same number of samples for all texels in the image, and the sample locations will not depend on the internal format or size of the image
 *
 * Establish the data storage, format, dimensions, and number of samples of a multisample texture's image.
 */
void
updwn_texImage3DMultisample (UpdwnTextureTarget target, int samples, UpdwnPixelInternalFormat internalformat, int width, int height, int depth, bool fixedsamplelocations)
{
  glTexImage3DMultisample (target, samples, internalformat, width, height, depth, fixedsamplelocations);
}

/*
 * glUniform family
 */

#define UNIFORM1_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (int location, data_type v0) \
{ \
  gl_func (location, v0); \
}

#define UNIFORM2_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (int location, data_type v0, data_type v1) \
{ \
  gl_func (location, v0, v1); \
}

#define UNIFORM3_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (int location, data_type v0, data_type v1, data_type v2) \
{ \
  gl_func (location, v0, v1, v2); \
}

#define UNIFORM4_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (int location, data_type v0, data_type v1, data_type v2, data_type v3) \
{ \
  gl_func (location, v0, v1, v2, v3); \
}

/**
 * updwn_uniform1f:
 * @location: the location of the uniform variable to be modified
 * @v0: the first new value to be used for the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM1_TYPE (updwn_uniform1f, float, glUniform1f)
UNIFORM2_TYPE (updwn_uniform2f, float, glUniform2f)
UNIFORM3_TYPE (updwn_uniform3f, float, glUniform3f)
UNIFORM4_TYPE (updwn_uniform4f, float, glUniform4f)

UNIFORM1_TYPE (updwn_uniform1i, int, glUniform1i)
UNIFORM2_TYPE (updwn_uniform2i, int, glUniform2i)
UNIFORM3_TYPE (updwn_uniform3i, int, glUniform3i)
UNIFORM4_TYPE (updwn_uniform4i, int, glUniform4i)

UNIFORM1_TYPE (updwn_uniform1ui, uint, glUniform1ui)
UNIFORM2_TYPE (updwn_uniform2ui, uint, glUniform2ui)
UNIFORM3_TYPE (updwn_uniform3ui, uint, glUniform3ui)
UNIFORM4_TYPE (updwn_uniform4ui, uint, glUniform4ui)

#undef UNIFORM1_TYPE
#undef UNIFORM2_TYPE
#undef UNIFORM3_TYPE
#undef UNIFORM4_TYPE

#define UNIFORM_TYPEV(updwn_func, n, data_type, gl_func) \
void \
updwn_func (int location, int length, const data_type *value) \
{ \
  int count = length / n; \
  gl_func (location, count, value); \
}

/**
 * updwn_uniform1fv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform1fv, 1, float, glUniform1fv)

/**
 * updwn_uniform2fv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform2fv, 2, float, glUniform2fv)

/**
 * updwn_uniform3fv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform3fv, 3, float, glUniform3fv)

/**
 * updwn_uniform4fv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform4fv, 4, float, glUniform4fv)

/**
 * updwn_uniform1iv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform1iv, 1, int, glUniform1iv)

/**
 * updwn_uniform2iv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform2iv, 2, int, glUniform2iv)

/**
 * updwn_uniform3iv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform3iv, 3, int, glUniform3iv)

/**
 * updwn_uniform4iv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform4iv, 4, int, glUniform4iv)

/**
 * updwn_uniform1uiv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform1uiv, 1, uint, glUniform1uiv)

/**
 * updwn_uniform2uiv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform2uiv, 2, uint, glUniform2uiv)

/**
 * updwn_uniform3uiv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform3uiv, 3, uint, glUniform3uiv)

/**
 * updwn_uniform4uiv:
 * @location: the location of the uniform variable to be modified
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_TYPEV (updwn_uniform4uiv, 4, uint, glUniform4uiv)

#undef UNIFORM_TYPEV

#define UNIFORM_MATRIX(updwn_func, dimension, gl_func) \
void \
updwn_func (int location, bool transpose, int length, const float *value) \
{ \
  int count = length / (dimension); \
  gl_func (location, count, transpose, value); \
}

/**
 * updwn_uniformMatrix2fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix2fv, 2 * 2, glUniformMatrix2fv)

/**
 * updwn_uniformMatrix3fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix3fv, 3 * 3, glUniformMatrix3fv)

/**
 * updwn_uniformMatrix4fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix4fv, 4 * 4, glUniformMatrix4fv)

/**
 * updwn_uniformMatrix2x3fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix2x3fv, 2 * 3, glUniformMatrix2x3fv)

/**
 * updwn_uniformMatrix3x2fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix3x2fv, 3 * 2, glUniformMatrix3x2fv)

/**
 * updwn_uniformMatrix2x4fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix2x4fv, 2 * 4, glUniformMatrix2x4fv)

/**
 * updwn_uniformMatrix4x2fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix4x2fv, 4 * 2, glUniformMatrix4x2fv)

/**
 * updwn_uniformMatrix3x4fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix3x4fv, 3 * 4, glUniformMatrix3x4fv)

/**
 * updwn_uniformMatrix4x3fv:
 * @location: the location of the uniform variable to be modified
 * @transpose: whether to transpose the matrix as the values are loaded into the uniform variable
 * @length: the length of @value
 * @value: (array length=length): a pointer to an array of values that will be used to update the specified uniform variable
 *
 * Specify the value of a uniform variable for the current program object.
 */
UNIFORM_MATRIX (updwn_uniformMatrix4x3fv, 4 * 3, glUniformMatrix4x3fv)

#undef UNIFORM_MATRIX

/**
 * updwn_useProgram:
 * @program: the handle of the program object whose executables are to be used as part of current rendering state
 *
 * Install a program object as part of current rendering state.
 */
void
updwn_useProgram (uint program)
{
  glUseProgram (program);
}

/**
 * updwn_validateProgram
 * @program: the handle of the program object to be validated
 *
 * Validate a program object.
 */
void
updwn_validateProgram (uint program)
{
  glValidateProgram (program);
}

/**
 * updwn_vertexArrayAttribBinding
 * @vaobj: the name of the vertex array object
 * @attribindex: the index of the attribute to associate with a vertex buffer binding
 * @bindingindex: the index of the vertex buffer binding with which to associate the generic vertex attribute
 *
 * Associate a vertex attribute and a vertex buffer binding for a vertex array object.
 *
 * OpenGL: 4.5
 */
void
updwn_vertexArrayAttribBinding (uint vaobj, uint attribindex, uint bindingindex)
{
  glVertexArrayAttribBinding (vaobj, attribindex, bindingindex);
}

/**
 * updwn_vertexArrayAttribFormat:
 * @vaobj: the name of the vertex array object
 * @attribindex: the generic vertex attribute array being described
 * @size: the number of values per vertex that are stored in the array
 * @type: the type of the data stored in the array
 * @normalized: whether fixed-point data values should be normalized (%TRUE) or converted directly as fixed-point values (%FALSE) when they are accessed. The parameter is ignored if type is `GL_FIXED`.
 * @relativeoffset: the distance between elements within the buffer
 *
 * Specify the organization of vertex arrays.
 *
 * OpenGL: 4.5
 */

void
updwn_vertexArrayAttribFormat (uint vaobj, uint attribindex, int size, UpdwnDataType type, bool normalized, uint relativeoffset)
{
  glVertexArrayAttribFormat (vaobj, attribindex, size, type, normalized, relativeoffset);
}

/**
 * updwn_vertexArrayAttribIFormat:
 * @vaobj: the name of the vertex array object
 * @attribindex: the generic vertex attribute array being described
 * @size: the number of values per vertex that are stored in the array
 * @type: the type of the data stored in the array
 * @relativeoffset: the distance between elements within the buffer
 *
 * Specify the organization of vertex arrays.
 *
 * OpenGL: 4.5
 */

void
updwn_vertexArrayAttribIFormat (uint vaobj, uint attribindex, int size, UpdwnDataType type, uint relativeoffset)
{
  glVertexArrayAttribIFormat (vaobj, attribindex, size, type, relativeoffset);
}

/**
 * updwn_vertexArrayAttribLFormat:
 * @vaobj: the name of the vertex array object
 * @attribindex: the generic vertex attribute array being described
 * @size: the number of values per vertex that are stored in the array
 * @type: the type of the data stored in the array, must be `GL_DOUBLE`
 * @relativeoffset: the distance between elements within the buffer
 *
 * Specify the organization of vertex arrays.
 *
 * OpenGL: 4.5
 */

void
updwn_vertexArrayAttribLFormat (uint vaobj, uint attribindex, int size, UpdwnDataType type, uint relativeoffset)
{
  glVertexArrayAttribLFormat (vaobj, attribindex, size, type, relativeoffset);
}

/**
 * updwn_vertexArrayBindingDivisor
 * @vaobj: the name of the vertex array object
 * @bindingindex: the index of the binding whose divisor to modify
 * @divisor: the new value for the instance step rate to apply
 *
 * Modify the rate at which generic vertex attributes advance.
 *
 * OpenGL: 4.5
 */
void
updwn_vertexArrayBindingDivisor (uint vaobj, uint bindingindex, uint divisor)
{
  glVertexArrayBindingDivisor (vaobj, bindingindex, divisor);
}

/**
 * updwn_vertexArrayVertexBuffer:
 * @vaobj: the name of the vertex array object to be used
 * @bindingindex: the index of the vertex buffer binding point to which to bind the buffer
 * @buffer: the name of a buffer to bind to the vertex buffer binding point
 * @offset: the offset in bytes of the first element of the buffer
 * @stride: the distance in bytes between elements within the buffer
 *
 * Bind a buffer to a vertex buffer bind point.
 *
 * OpenGL: 4.5
 */
void
updwn_vertexArrayVertexBuffer (uint vaobj, uint bindingindex, uint buffer, signed long offset, int stride)
{
  glVertexArrayVertexBuffer (vaobj, bindingindex, buffer, offset, stride);
}

/**
 * updwn_vertexArrayVertexBuffers:
 * @vaobj: the name of the vertex array object
 * @first: the first vertex buffer binding point to which a buffer object is to be bound
 * @count: the number of buffers to bind
 * @buffers: (array length=count): the address of an array of names of existing buffer objects
 * @offsets: (array): the address of an array of offsets (in bytes) to associate with the binding points
 * @strides: (array): the address of an array of strides (in bytes) to associate with the binding points
 *
 * Attach multiple buffer objects to a vertex array object.
 *
 * OpenGL: 4.5
 */
/* XXX: same as for bindBuffersBase */
void
updwn_vertexArrayVertexBuffers (uint vaobj, uint first, int count, const uint *buffers, const signed long *offsets, const int *strides)
{
  glVertexArrayVertexBuffers (vaobj, first, count, buffers, offsets, strides);
}

/*
 * glVertexAttrib family
 */

#define VERTEX_ATTRIB1_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (uint index, data_type v0) \
{ \
  gl_func (index, v0); \
}

#define VERTEX_ATTRIB2_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (uint index, data_type v0, data_type v1) \
{ \
  gl_func (index, v0, v1); \
}

#define VERTEX_ATTRIB3_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (uint index, data_type v0, data_type v1, data_type v2) \
{ \
  gl_func (index, v0, v1, v2); \
}

#define VERTEX_ATTRIB4_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (uint index, data_type v0, data_type v1, data_type v2, data_type v3) \
{ \
  gl_func (index, v0, v1, v2, v3); \
}

#define VERTEX_ATTRIBP_TYPE(updwn_func, data_type, gl_func) \
void \
updwn_func (uint index, UpdwnDataType type, bool normalized, data_type value) \
{ \
  gl_func (index, type, normalized, value); \
}

#define VERTEX_ATTRIB_TYPEV(updwn_func, data_type, gl_func) \
void \
updwn_func (uint index, const data_type *v) \
{ \
  gl_func (index, v); \
}

/**
 * updwn_vertexAttrib1d:
 * @index: the index of the generic vertex attribute to be modified
 * @v0: the first value to be used for the specified vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB1_TYPE (updwn_vertexAttrib1d, double, glVertexAttrib1d)
VERTEX_ATTRIB1_TYPE (updwn_vertexAttrib1f, float, glVertexAttrib1f)
VERTEX_ATTRIB1_TYPE (updwn_vertexAttrib1s, int16_t, glVertexAttrib1s)
VERTEX_ATTRIB2_TYPE (updwn_vertexAttrib2d, double, glVertexAttrib2d)
VERTEX_ATTRIB2_TYPE (updwn_vertexAttrib2f, float, glVertexAttrib2f)
VERTEX_ATTRIB2_TYPE (updwn_vertexAttrib2s, int16_t, glVertexAttrib2s)
VERTEX_ATTRIB3_TYPE (updwn_vertexAttrib3d, double, glVertexAttrib3d)
VERTEX_ATTRIB3_TYPE (updwn_vertexAttrib3f, float, glVertexAttrib3f)
VERTEX_ATTRIB3_TYPE (updwn_vertexAttrib3s, int16_t, glVertexAttrib3s)
VERTEX_ATTRIB4_TYPE (updwn_vertexAttrib4d, double, glVertexAttrib4d)
VERTEX_ATTRIB4_TYPE (updwn_vertexAttrib4f, float, glVertexAttrib4f)
VERTEX_ATTRIB4_TYPE (updwn_vertexAttrib4s, int16_t, glVertexAttrib4s)

/**
 * updwn_vertexAttrib4Nub:
 * @index: the index of the generic vertex attribute to be modified
 * @v0: the first value to be used for the specified vertex attribute
 * @v1: the second value to be used for the specified vertex attribute
 * @v2: the third value to be used for the specified vertex attribute
 * @v3: the fourth value to be used for the specified vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `N` indicates that the arguments will be passed as fixed-point values that are scaled to a normalized range (`[0,1]` here).
 */
VERTEX_ATTRIB4_TYPE (updwn_vertexAttrib4Nub, uint8_t, glVertexAttrib4Nub)

/**
 * updwn_vertexAttribI1i:
 * @index: the index of the generic vertex attribute to be modified
 * @v0: the first value to be used for the specified vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full signed or unsigned integers (full signed integers here).
 */
VERTEX_ATTRIB1_TYPE (updwn_vertexAttribI1i, int, glVertexAttribI1i)
VERTEX_ATTRIB1_TYPE (updwn_vertexAttribI1ui, uint, glVertexAttribI1ui)
VERTEX_ATTRIB2_TYPE (updwn_vertexAttribI2i, int, glVertexAttribI2i)
VERTEX_ATTRIB2_TYPE (updwn_vertexAttribI2ui, uint, glVertexAttribI2ui)
VERTEX_ATTRIB3_TYPE (updwn_vertexAttribI3i, int, glVertexAttribI3i)
VERTEX_ATTRIB3_TYPE (updwn_vertexAttribI3ui, uint, glVertexAttribI3ui)
VERTEX_ATTRIB4_TYPE (updwn_vertexAttribI4i, int, glVertexAttribI4i)
VERTEX_ATTRIB4_TYPE (updwn_vertexAttribI4ui, uint, glVertexAttribI4ui)

/**
 * updwn_vertexAttribL1d:
 * @index: the index of the generic vertex attribute to be modified
 * @v0: the first value to be used for the specified vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `L` indicates that the arguments are full 64-bit quantities and should be passed directly to shader inputs declared as 64-bit double precision types.
 */
VERTEX_ATTRIB1_TYPE (updwn_vertexAttribL1d, double, glVertexAttribL1d)
VERTEX_ATTRIB2_TYPE (updwn_vertexAttribL2d, double, glVertexAttribL2d)
VERTEX_ATTRIB3_TYPE (updwn_vertexAttribL3d, double, glVertexAttribL3d)
VERTEX_ATTRIB4_TYPE (updwn_vertexAttribL4d, double, glVertexAttribL4d)

/**
 * updwn_vertexAttrib1dv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib1dv, double, glVertexAttrib1dv)

/**
 * updwn_vertexAttrib1fv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib1fv, float, glVertexAttrib1fv)

/**
 * updwn_vertexAttrib1sv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib1sv, int16_t, glVertexAttrib1sv)

/**
 * updwn_vertexAttrib2dv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib2dv, double, glVertexAttrib2dv)

/**
 * updwn_vertexAttrib2fv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib2fv, float, glVertexAttrib2fv)

/**
 * updwn_vertexAttrib2sv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib2sv, int16_t, glVertexAttrib2sv)

/**
 * updwn_vertexAttrib3dv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib3dv, double, glVertexAttrib3dv)

/**
 * updwn_vertexAttrib3fv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib3fv, float, glVertexAttrib3fv)

/**
 * updwn_vertexAttrib3sv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib3sv, int16_t, glVertexAttrib3sv)

/**
 * updwn_vertexAttrib4bv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4bv, int8_t, glVertexAttrib4bv)

/**
 * updwn_vertexAttrib4dv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4dv, double, glVertexAttrib4dv)

/**
 * updwn_vertexAttrib4fv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4fv, float, glVertexAttrib4fv)

/**
 * updwn_vertexAttrib4iv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4iv, int, glVertexAttrib4iv)

/**
 * updwn_vertexAttrib4sv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4sv, int16_t, glVertexAttrib4sv)

/**
 * updwn_vertexAttrib4ubv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4ubv, uint8_t, glVertexAttrib4ubv)

/**
 * updwn_vertexAttrib4uiv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4uiv, uint, glVertexAttrib4uiv)

/**
 * updwn_vertexAttrib4usv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4usv, uint16_t, glVertexAttrib4usv)

/**
 * updwn_vertexAttrib4Nbv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `N` indicates that the arguments will be passed as fixed-point values that are scaled to the normalized range `[-1,1]`.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4Nbv, int8_t, glVertexAttrib4Nbv)

/**
 * updwn_vertexAttrib4Niv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `N` indicates that the arguments will be passed as fixed-point values that are scaled to the normalized range `[-1,1]`.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4Niv, int, glVertexAttrib4Niv)

/**
 * updwn_vertexAttrib4Nsv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `N` indicates that the arguments will be passed as fixed-point values that are scaled to the normalized range `[-1,1]`.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4Nsv, int16_t, glVertexAttrib4Nsv)

/**
 * updwn_vertexAttrib4Nubv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `N` indicates that the arguments will be passed as fixed-point values that are scaled to the normalized range `[0,1]`.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4Nubv, uint8_t, glVertexAttrib4Nubv)

/**
 * updwn_vertexAttrib4Nuiv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `N` indicates that the arguments will be passed as fixed-point values that are scaled to the normalized range `[0,1]`.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4Nuiv, uint, glVertexAttrib4Nuiv)

/**
 * updwn_vertexAttrib4Nusv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `N` indicates that the arguments will be passed as fixed-point values that are scaled to the normalized range `[0,1]`.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttrib4Nusv, uint16_t, glVertexAttrib4Nusv)

/**
 * updwn_vertexAttribI1iv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full signed integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI1iv, int, glVertexAttribI1iv)

/**
 * updwn_vertexAttribI1uiv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full unsigned integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI1uiv, uint, glVertexAttribI1uiv)

/**
 * updwn_vertexAttribI2iv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full signed integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI2iv, int, glVertexAttribI2iv)

/**
 * updwn_vertexAttribI2uiv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full unsigned integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI2uiv, uint, glVertexAttribI2uiv)

/**
 * updwn_vertexAttribI3iv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full signed integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI3iv, int, glVertexAttribI3iv)

/**
 * updwn_vertexAttribI3uiv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full unsigned integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI3uiv, uint, glVertexAttribI3uiv)

/**
 * updwn_vertexAttribI4bv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full signed integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI4bv, int8_t, glVertexAttribI4bv)

/**
 * updwn_vertexAttribI4iv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full signed integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI4iv, int, glVertexAttribI4iv)

/**
 * updwn_vertexAttribI4sv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full signed integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI4sv, int16_t, glVertexAttribI4sv)

/**
 * updwn_vertexAttribI4ubv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full unsigned integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI4ubv, uint8_t, glVertexAttribI4ubv)

/**
 * updwn_vertexAttribI4uiv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full unsigned integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI4uiv, uint, glVertexAttribI4uiv)

/**
 * updwn_vertexAttribI4usv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `I` indicates that the arguments are extended to full unsigned integers.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribI4usv, uint16_t, glVertexAttribI4usv)

/**
 * updwn_vertexAttribL1dv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `L` indicates that the arguments are full 64-bit quantities and should be passed directly to shader inputs declared as 64-bit double precision types.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribL1dv, double, glVertexAttribL1dv)

/**
 * updwn_vertexAttribL2dv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `L` indicates that the arguments are full 64-bit quantities and should be passed directly to shader inputs declared as 64-bit double precision types.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribL2dv, double, glVertexAttribL2dv)

/**
 * updwn_vertexAttribL3dv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `L` indicates that the arguments are full 64-bit quantities and should be passed directly to shader inputs declared as 64-bit double precision types.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribL3dv, double, glVertexAttribL3dv)

/**
 * updwn_vertexAttribL4dv:
 * @index: the index of the generic vertex attribute to be modified
 * @v: (array): a pointer to an array of values to be used for the generic vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `L` indicates that the arguments are full 64-bit quantities and should be passed directly to shader inputs declared as 64-bit double precision types.
 */
VERTEX_ATTRIB_TYPEV (updwn_vertexAttribL4dv, double, glVertexAttribL4dv)

/**
 * updwn_vertexAttribP1ui:
 * @index: the index of the generic vertex attribute to be modified
 * @type: the type of packing used on the data
 * @normalized: if %TRUE, then the values are to be converted to floating point values by normalizing. Otherwise, they are converted directly to floating-point values. If @type indicates a floating-pont format, then @normalized value must be %FALSE.
 * @value: the new packed value to be used for the specified vertex attribute
 *
 * Specify the value of a generic vertex attribute.
 *
 * `P` indicate that the arguments are stored as packed components within a larger natural type.
 */
VERTEX_ATTRIBP_TYPE (updwn_vertexAttribP1ui, uint, glVertexAttribP1ui)
VERTEX_ATTRIBP_TYPE (updwn_vertexAttribP2ui, uint, glVertexAttribP2ui)
VERTEX_ATTRIBP_TYPE (updwn_vertexAttribP3ui, uint, glVertexAttribP3ui)
VERTEX_ATTRIBP_TYPE (updwn_vertexAttribP4ui, uint, glVertexAttribP4ui)

#undef VERTEX_ATTRIB1_TYPE
#undef VERTEX_ATTRIB2_TYPE
#undef VERTEX_ATTRIB3_TYPE
#undef VERTEX_ATTRIB4_TYPE
#undef VERTEX_ATTRIBP_TYPE
#undef VERTEX_ATTRIB_TYPEV

/**
 * updwn_vertexAttribBinding
 * @attribindex: the index of the attribute to associate with a vertex buffer binding
 * @bindingindex: the index of the vertex buffer binding with which to associate the generic vertex attribute
 *
 * Associate a vertex attribute and a vertex buffer binding for a vertex array object.
 *
 * The vertex array object affected is that currently bound. Use updwn_vertexArrayAttribBinding() variant to specify the vertex array object.
 */
void
updwn_vertexAttribBinding (uint attribindex, uint bindingindex)
{
  glVertexAttribBinding (attribindex, bindingindex);
}

/**
 * updwn_vertexAttribFormat:
 * @attribindex: the generic vertex attribute array being described
 * @size: the number of values per vertex that are stored in the array
 * @type: the type of the data stored in the array
 * @normalized: whether fixed-point data values should be normalized (%TRUE) or converted directly as fixed-point values (%FALSE) when they are accessed. The parameter is ignored if type is `GL_FIXED`.
 * @relativeoffset: the distance between elements within the buffer
 *
 * Specify the organization of vertex arrays.
 *
 * The vertex array object affected is that currently bound. Use updwn_vertexArrayAttribFormat() variant to specify the vertex array object.
 */

void
updwn_vertexAttribFormat (uint attribindex, int size, UpdwnDataType type, bool normalized, uint relativeoffset)
{
  glVertexAttribFormat (attribindex, size, type, normalized, relativeoffset);
}

/**
 * updwn_vertexAttribIFormat:
 * @attribindex: the generic vertex attribute array being described
 * @size: the number of values per vertex that are stored in the array
 * @type: the type of the data stored in the array
 * @relativeoffset: the distance between elements within the buffer
 *
 * Specify the organization of vertex arrays.
 *
 * The vertex array object affected is that currently bound. Use updwn_vertexArrayAttribIFormat() variant to specify the vertex array object.
 */

void
updwn_vertexAttribIFormat (uint attribindex, int size, UpdwnDataType type, uint relativeoffset)
{
  glVertexAttribIFormat (attribindex, size, type, relativeoffset);
}

/**
 * updwn_vertexAttribIPointer:
 * @index: the index of the generic vertex attribute to be modified
 * @size: the number of components per generic vertex attribute, must be 1, 2, 3, 4 or the symbolic constant `GL_BGRA`
 * @type: the data type of each component in the array
 * @stride: the byte offset between consecutive generic vertex attributes. If stride is 0, the generic vertex attributes are understood to be tightly packed in the array.
 * @pointer: (type gint): a offset of the first component of the first generic vertex attribute in the array in the data store of the buffer currently bound to the `GL_ARRAY_BUFFER` target
 *
 * Define an array of generic vertex attribute data.
 *
 * <note>Use updwn_get_data_type_size() to get the size of the array elements.</note>
 */
void
updwn_vertexAttribIPointer (uint index, int size, UpdwnDataType type, int stride, const void *pointer)
{
  glVertexAttribIPointer (index, size, type, stride, pointer);
}

/**
 * updwn_vertexAttribLFormat:
 * @attribindex: the generic vertex attribute array being described
 * @size: the number of values per vertex that are stored in the array
 * @type: the type of the data stored in the array, must be `GL_DOUBLE`
 * @relativeoffset: the distance between elements within the buffer
 *
 * Specify the organization of vertex arrays.
 *
 * The vertex array object affected is that currently bound. Use updwn_vertexArrayAttribLFormat() variant to specify the vertex array object.
 */

void
updwn_vertexAttribLFormat (uint attribindex, int size, UpdwnDataType type, uint relativeoffset)
{
  glVertexAttribLFormat (attribindex, size, type, relativeoffset);
}

/**
 * updwn_vertexAttribLPointer:
 * @index: the index of the generic vertex attribute to be modified
 * @size: the number of components per generic vertex attribute, must be 1, 2, 3 or 4
 * @type: the data type of each component in the array, must be `GL_DOUBLE`
 * @stride: the byte offset between consecutive generic vertex attributes. If stride is 0, the generic vertex attributes are understood to be tightly packed in the array.
 * @pointer: (type gint): a offset of the first component of the first generic vertex attribute in the array in the data store of the buffer currently bound to the `GL_ARRAY_BUFFER` target
 *
 * Define an array of generic vertex attribute data.
 *
 * <note>Use updwn_get_data_type_size() to get the size of the array elements.</note>
 */
void
updwn_vertexAttribLPointer (uint index, int size, UpdwnDataType type, int stride, const void *pointer)
{
  glVertexAttribLPointer (index, size, type, stride, pointer);
}

/**
 * updwn_vertexAttribPointer:
 * @index: the index of the generic vertex attribute to be modified
 * @size: the number of components per generic vertex attribute, must be 1, 2, 3 or 4
 * @type: the data type of each component in the array. It must be an integer type.
 * @normalized: whether fixed-point data values should be normalized (%TRUE) or converted directly as fixed-point values (%FALSE) when they are accessed
 * @stride: the byte offset between consecutive generic vertex attributes. If stride is 0, the generic vertex attributes are understood to be tightly packed in the array.
 * @pointer: (type gint): a offset of the first component of the first generic vertex attribute in the array in the data store of the buffer currently bound to the `GL_ARRAY_BUFFER` target
 *
 * Define an array of generic vertex attribute data.
 *
 * <note>Use updwn_get_data_type_size() to get the size of the array elements.</note>
 */
void
updwn_vertexAttribPointer (uint index, int size, UpdwnDataType type, bool normalized, int stride, const void *pointer)
{
  glVertexAttribPointer (index, size, type, normalized, stride, pointer);
}

/**
 * updwn_vertexBindingDivisor
 * @bindingindex: the index of the binding whose divisor to modify
 * @divisor: the new value for the instance step rate to apply
 *
 * Modify the rate at which generic vertex attributes advance.
 *
 * The vertex array object affected is that currently bound. Use updwn_vertexArrayBindingDivisor() variant to specify the vertex array object.
 */
void
updwn_vertexBindingDivisor (uint bindingindex, uint divisor)
{
  glVertexBindingDivisor (bindingindex, divisor);
}

/**
 * updwn_viewportArrayv:
 * @first: the first viewport to set
 * @length: the length of @v
 * @v: (array length=length): an array containing the viewport parameters
 *
 * Set multiple viewports.
 *
 * <note>@length must be equal to `4 x count` where `count` is the number of viewports to set.</note>
 */
void
updwn_viewportArrayv (uint first, int length, const float *v)
{
  int count = length / 4;
  glViewportArrayv (first, count, v);
}

/**
 * updwn_viewportIndexedf:
 * @index: the first viewport to set
 * @x: the x coordinate of the lower left corner of the viewport rectangle, in pixels
 * @y: the y coordinate of the lower left corner of the viewport rectangle, in pixels
 * @width: the width of the viewport
 * @height: the height of the viewport
 *
 * Set a specified viewport.
 *
 * When a GL context is first attached to a window, @width and @height are set to the dimensions of that window.
 */
void
updwn_viewportIndexedf (uint index, float x, float y, float width, float height)
{
  glViewportIndexedf (index, x, y, width, height);
}

/**
 * updwn_viewportIndexedfv:
 * @index: the first viewport to set
 * @v: (array): an array containing the viewport parameters
 *
 * Set a specified viewport.
 */
void
updwn_viewportIndexedfv (uint index, const float *v)
{
  glViewportIndexedfv (index, v);
}
