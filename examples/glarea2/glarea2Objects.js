/*
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

const { Gio, GLib, GObject, Updwn: gl } = imports.gi;
const ByteArray = imports.byteArray;

var GlObjectBase = GObject.registerClass({
    Properties: {
        'id': GObject.ParamSpec.uint(
            'id', 'Id', 'GL identifier',
            GObject.ParamFlags.READABLE,
            0, GLib.MAXUINT32, 0
        ),
        'label': GObject.ParamSpec.string(
            'label', 'Label', 'Debug Label',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            null
        ),
    },
}, class GlObject extends GObject.Object {
    vfunc_constructed() {
        if (this.label)
            gl.objectLabel(this.constructor.objectType, this.id, this.label);
    }

    get id() {
        return this._id;
    }

    get _id() {
        return this.__id || 0;
    }

    set _id(id) {
        if (this.__id != id) {
            this.__id = id;
            this.notify('id');
        }
    }
});

var Shader = GObject.registerClass({
    Properties: {
        'compiled': GObject.ParamSpec.boolean(
            'compiled', 'compiled', 'Compiled',
            GObject.ParamFlags.READABLE,
            false
        ),
        'file': GObject.ParamSpec.object(
            'file', 'File', 'Source file',
            GObject.ParamFlags.WRITABLE,
            Gio.File.$gtype
        ),
        'source': GObject.ParamSpec.string(
            'source', 'Source', 'Source',
            GObject.ParamFlags.READWRITE,
            null
        ),
        'type': GObject.ParamSpec.enum(
            'type', 'Type', 'Type',
            GObject.ParamFlags.READWRITE | GObject.ParamFlags.CONSTRUCT_ONLY,
            gl.ShaderType.$gtype, gl.ShaderType.VERTEX
        ),
    },
}, class Shader extends GlObjectBase {
    static objectType = gl.ObjectType.SHADER;

    vfunc_constructed() {
        this._id = gl.createShader(this.type);
        super.vfunc_constructed();
    }

    get source() {
        return ByteArray.toString(gl.getShaderSource(this.id, -1));
    }

    set source(source) {
        if (this._compiled)
            throw new Error("Cannot add new source");

        try {
            gl.shaderSource(this.id, [source], null);

            gl.compileShader(this.id);
            let [status] = gl.getShaderiv(this.id, gl.ShaderParam.COMPILE_STATUS);
            if (!status) {
                let infoLog = gl.getShaderInfoLog(this.id, -1);
                throw new Error(`Compile failure in shader:\n${ByteArray.toString(infoLog)}`);
            }

            this._compiled = true;
            this.notify('compiled');
        } catch(e) {
            this.delete();
            throw e;
        }
    }

    set file(file) {
        let contents = file.load_contents(null)[1];
        this.source = ByteArray.toString(contents);
    }

    get compiled() {
        return this._compiled || false;
    }

    delete() {
        gl.deleteShader(this.id);
        this._id = 0;
    }
});

// An object container to pass a list of shaders to a program via a property.
var ShaderList = class {
    constructor(...shaders) {
        let listStore = new Gio.ListStore({ itemType: Shader.$gtype });
        shaders.forEach(shader => listStore.append(shader));
        return listStore;
    }
};

var Program = GObject.registerClass({
    Properties: {
        'shaders': GObject.ParamSpec.object(
            'shaders', 'Shaders', 'Shaders',
            GObject.ParamFlags.WRITABLE,
            Gio.ListStore.$gtype
        ),
    },
}, class Program extends GlObjectBase {
    static objectType = gl.ObjectType.PROGRAM;

    vfunc_constructed() {
        this._id = gl.createProgram();
        super.vfunc_constructed();
    }

    set shaders(shaders) {
        if (this._shadersAdded)
            throw new Error("Cannot add new shaders");

        for (let i = 0; i < shaders.get_n_items(); i++) {
            let shader = shaders.get_item(i);
            if (!shader.compiled) {
                this._detachAll();
                throw new Error(`The shader ${shader.id} is not compiled`);
            }

            gl.attachShader(this.id, shader.id);
        }

        gl.linkProgram(this.id);

        let [status] = gl.getProgramiv(this.id, gl.ProgramParam.LINK_STATUS);
        if (!status) {
            let infoLog = gl.getProgramInfoLog(this.id, -1);
            this.delete();
            throw new Error(`Linking failure:\n${ByteArray.toString(infoLog)}`);
        }

        for (let i = 0; i < shaders.get_n_items(); i++)
            gl.detachShader(this.id, shaders.get_item(i).id);

        this._shadersAdded = true;
    }

    _detachAll() {
        gl.getAttachedShaders(this.id, -1).forEach(shaderId => gl.detachShader(this.id, shaderId));
    }

    getUniformLocation(name) {
        let location = gl.getUniformLocation(this.id, name);
        if (location < 0)
            logError(new Error(`${name} uniform location not found in program ${this.id}`));

        return location;
    }

    getAttribLocation(name) {
        let location = gl.getAttribLocation(this.id, name);
        if (location < 0)
            logError(new Error(`${name} attribute location not found in program ${this.id}`));

        return location;
    }

    use() {
        gl.useProgram(this.id);
    }

    unuse() {
        gl.useProgram(0);
    }

    setMatrix4(name, transpose, matrix) {
        let location = gl.getUniformLocation(this.id, name);
        gl.uniformMatrix4fv(location, false, matrix);
    }

    delete() {
        gl.deleteProgram(this.id);
        this._id = 0;
    }
});

var Buffer = GObject.registerClass({
    Properties: {
        'binding-target': GObject.ParamSpec.enum(
            'binding-target', 'Binding Target', 'The binding target',
            GObject.ParamFlags.READWRITE,
            gl.BufferTarget.$gtype, gl.BufferTarget.ARRAY
        ),
        'data-type': GObject.ParamSpec.enum(
            'data-type', 'Data Type', 'The type of the data',
            GObject.ParamFlags.READWRITE,
            gl.DataType, gl.DataType.FLOAT
        ),
        'usage': GObject.ParamSpec.enum(
            'usage', 'Usage', 'The data store usage',
            GObject.ParamFlags.READWRITE,
            gl.BufferDataStoreUsage, gl.BufferDataStoreUsage.STATIC_DRAW
        ),
    },
}, class Buffer extends GlObjectBase {
    static objectType = gl.ObjectType.BUFFER;

    vfunc_constructed() {
        [this._id] = gl.genBuffers(1);
        super.vfunc_constructed();
    }

    get binding_target() {
        return this._bindingTarget || gl.BufferTarget.ARRAY;
    }

    set binding_target(bindingTarget) {
        if (this._bindingTargetLocked)
            throw new Error("Cannot change the buffer binding target");

        if (this._bindingTarget != bindingTarget) {
            this._bindingTarget = bindingTarget;
            this.notify('binding-target');
        }
    }

    get data_type() {
        return this._dataType || gl.DataType.FLOAT;
    }

    set data_type(dataType) {
        if (this._dataTypeLocked)
            throw new Error("Cannot change the buffer data type");

        if (this._dataType != dataType) {
            this._dataType = dataType;
            this.notify('data-type');
        }
    }

    bind() {
        this._bindingTargetLocked = true;

        gl.bindBuffer(this.binding_target, this.id);
    }

    unbind() {
        gl.bindBuffer(this.binding_target, 0);
    }

    addData(data) {
        this.bind();
        gl.bufferDatafv(this.binding_target, data, this.usage);
        this.unbind();
    }

    setAttribute(index, size, normalized, stride, offset) {
        if (index < 0)
            return;

        this._dataTypeLocked = true;

        let dataTypeSize = gl.get_data_type_size(this.data_type);

        this.bind();
        gl.vertexAttribPointer(index, size, this.data_type, normalized, stride * dataTypeSize, offset * dataTypeSize);
        gl.enableVertexAttribArray(index);
        this.unbind();
    }

    delete() {
        gl.deleteBuffers([this.id]);
        this._id = 0;
    }
});

var VertexArray = GObject.registerClass({
}, class VertexArray extends GlObjectBase {
    static objectType = gl.ObjectType.VERTEX_ARRAY;

    vfunc_constructed() {
        [this._id] = gl.genVertexArrays(1);
        super.vfunc_constructed();
    }

    bind() {
        gl.bindVertexArray(this.id);
    }

    unbind() {
        gl.bindVertexArray(0);
    }

    draw(mode, first, count) {
        this.bind();
        gl.drawArrays(mode, first, count);
        this.unbind();
    }

    delete() {
        gl.deleteVertexArrays([this.id]);
        this._id = 0;
    }
});
