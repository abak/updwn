#version 330 core
in vec3 fColor;
in vec2 fTexcoord;
uniform sampler2D tex;
out vec4 outputColor;
void main()
{
    outputColor = texture(tex, fTexcoord) * vec4(fColor, 1.0);
}
